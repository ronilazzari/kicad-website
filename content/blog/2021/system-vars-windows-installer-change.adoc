+++
title = "System variables and Windows nightly installer change"
date = "2021-01-01"
draft = true
"blog/categories" = [
    "News"
]
+++

KiCad has renamed its System variables in use for nightly builds that will carry into KiCad 6.
This has become necessary to allow concurrent versions of KiCad to more easily co-exist on platforms in the future.

<!--more-->

== System Variables Changes

[options="header",role="table table-striped table-condensed"]
|===
| Old                 | New
| KISYSMOD            | KICAD6_FOOTPRINT_DIR
| KISYS3DMOD          | KICAD6_3DMODEL_DIR
| KICAD_SYMBOL_DIR    | KICAD6_SYMBOL_DIR
| KICAD_PTEMPLATES    | KICAD6_TEMPLATE_DIR
|===

=== kicad_pcb changes required
Unfortunately, the `KISYS3DMOD` variable gets saved into your kicad_pcb file when referencing footprints.

If you already had a KiCad nightly install, the old and new variables will co-exist in your KiCad settings.

All fresh install users will need to either:

[loweralpha]
a.  keep and maintain `KISYS3DMOD`. This can be done by launching KiCad, going to the *Preferences > Configure Paths* menu. +
     Create a new entry with `KISYS3DMOD` pointed to where 3d models are stored,  which will most likely be where `KICAD6_FOOTPRINT_DIR` is pointed to +
*OR*
b.  migrate their kicad_pcb by using Search and Replace in a text editor to swap the variable. This is perfectly safe to do.


== Windows Installer Change

The Windows installer for new KiCad nightly builds from today onwards will now install into a versioned path.

Previously, KiCad would be installed into

`C:\Program Files\KiCad\`

Now KiCad nightly will install into

`C:\Program Files\KiCad\5.99\`

Users are encouraged to uninstall the old nightly builds first before installing the new one as this will "jumble things up" in the folder.

If this was already done, you may simply delete C:\Program Files\KiCad and reinstall the latest nightly without harm.

This does mean you can now have 5.1 and the 5.99 (6.0) nightlies concurrently installed with ease. 