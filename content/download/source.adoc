+++
title = "Install from Source Code"
summary = "Install instructions for KiCad from source code"
+++

The most recent source code for KiCad can be downloaded from the
https://gitlab.com/kicad/code/kicad[GitLab repository].

See the https://dev-docs.kicad.org/en/build/[developer documentation] site for instructions on
how to build KiCad from source.  You may also want to look at one of the existing
https://gitlab.com/kicad/packaging/[packaging repositories] to see how KiCad is built and
packaged for our officially-supported platforms.

== Packaging KiCad

If you are interested in packaging KiCad for a new platform, please contact the KiCad core team via
the https://groups.google.com/a/kicad.org/g/devlist[developers mailing list] to let us know about
your efforts and so that we can provide guidance and support.
