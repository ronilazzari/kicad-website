+++
title = "Install on Arch Linux"
summary = "Install instructions for KiCad on Arch Linux"
+++

== Stable Release
{{< repology arch >}}

KiCad is available in https://www.archlinux.org/packages/community/x86_64/kicad/[`community/kicad`].

TIP: If you are looking to test out the latest development features we
recommend the use of the pre-built <<nightly>>, this makes it easier
to find and reproduce bugs, and allows coexistance with the stable
version.

To install KiCad you can run the following in a terminal:

[source,bash]
sudo pacman -Syu kicad
# if you want to install the official libraries (recommended):
sudo pacman -Syu --asdeps kicad-library kicad-library-3d

* `kicad-library`: a package containing official footprints, symbols and template for the official library
* `kicad-library-3d`: a package containing 3D models for the official library

`--asdeps` means the packages are installed as a dependecy of KiCad, meaning they will be uninstalled automatically when uninstalling it.

[#nightly]
== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a specific time.
This codebase is under active development, and while we try our best, may contain more bugs than usual.
New features added to KiCad can be tested in these builds.

WARNING: Please read link:/help/nightlies-and-rcs/[Nightly Builds and Release Candidates] for
		 important information about the risks and drawbacks of using nightly builds.

TIP: Nightly builds are provided in a separate installation directory. It is
possible to install nightly builds at the same time as the stable version.

Nightly development builds are available through the AUR in
https://aur.archlinux.org/packages/kicad-nightly/[`aur/kicad-nightly`].
This can be installed manually, you may simplify the installation
through the use of an
https://wiki.archlinux.org/title/AUR_helpers[AUR helper], note that
this process will compile the package and may take a long time.

Nightly development builds are also available in a binary repository, where they are automatically built periodically
https://kicad.gitlab.io/packaging/kicad-arch/kicad-arch-builder/[`kicad-arch-builder`]

You can have your system fetch and install these builds automatically
by configuring pacman (the package manager) and adding the custom
repository.  To do this you can append this entry to the end of the
pacman config file:

[source]
./etc/pacman.conf
----
# Nightly builds for KiCad
[kicad-nightly]
SigLevel = Optional TrustAll
Server = https://kicad.gitlab.io/packaging/kicad-arch/kicad-arch-builder/
----

Now you can update the package database and install the the latest nightly with:

[source,bash]
sudo pacman -Syu kicad-nightly

WARNING: The builds in this repository are currently not being signed.

NOTE: Currently library packages are not prebuilt due to some
constraints and must be installed through the AUR manually.

The nightly packaging scripts are maintained at Gitlab in the
https://gitlab.com/kicad/packaging/kicad-arch/kicad-arch-builder[`kicad-arch-builder`]
repo.

== Simple AUR package

A more "bare bones" packages is also availbele in
https://aur.archlinux.org/packages/kicad-git/[aur/kicad-git] to
install the latest development branch from source. You are likely
better off trying out the above nightly package.

CAUTION: This will conflict with the stable kicad installation.
